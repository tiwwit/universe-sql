--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Ubuntu 12.17-1.pgdg22.04+1)
-- Dumped by pg_dump version 12.17 (Ubuntu 12.17-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE worldcup;
--
-- Name: worldcup; Type: DATABASE; Schema: -; Owner: freecodecamp
--

CREATE DATABASE worldcup WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C.UTF-8' LC_CTYPE = 'C.UTF-8';


ALTER DATABASE worldcup OWNER TO freecodecamp;

\connect worldcup

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: games; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.games (
    game_id integer NOT NULL,
    winner_goals integer NOT NULL,
    opponent_goals integer NOT NULL,
    opponent_id integer NOT NULL,
    winner_id integer NOT NULL,
    year integer NOT NULL,
    round character varying(60) NOT NULL
);


ALTER TABLE public.games OWNER TO freecodecamp;

--
-- Name: games_game_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.games_game_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_game_id_seq OWNER TO freecodecamp;

--
-- Name: games_game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.games_game_id_seq OWNED BY public.games.game_id;


--
-- Name: teams; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.teams (
    team_id integer NOT NULL,
    name character varying(60) NOT NULL
);


ALTER TABLE public.teams OWNER TO freecodecamp;

--
-- Name: teams_team_id_seq; Type: SEQUENCE; Schema: public; Owner: freecodecamp
--

CREATE SEQUENCE public.teams_team_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teams_team_id_seq OWNER TO freecodecamp;

--
-- Name: teams_team_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: freecodecamp
--

ALTER SEQUENCE public.teams_team_id_seq OWNED BY public.teams.team_id;


--
-- Name: games game_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.games ALTER COLUMN game_id SET DEFAULT nextval('public.games_game_id_seq'::regclass);


--
-- Name: teams team_id; Type: DEFAULT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.teams ALTER COLUMN team_id SET DEFAULT nextval('public.teams_team_id_seq'::regclass);


--
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.games VALUES (1, 4, 2, 51, 50, 2018, 'Final');
INSERT INTO public.games VALUES (2, 2, 0, 53, 52, 2018, 'Third Place');
INSERT INTO public.games VALUES (3, 2, 1, 53, 51, 2018, 'Semi-Final');
INSERT INTO public.games VALUES (4, 1, 0, 52, 50, 2018, 'Semi-Final');
INSERT INTO public.games VALUES (5, 3, 2, 54, 51, 2018, 'Quarter-Final');
INSERT INTO public.games VALUES (6, 2, 0, 55, 53, 2018, 'Quarter-Final');
INSERT INTO public.games VALUES (7, 2, 1, 56, 52, 2018, 'Quarter-Final');
INSERT INTO public.games VALUES (8, 2, 0, 57, 50, 2018, 'Quarter-Final');
INSERT INTO public.games VALUES (9, 2, 1, 58, 53, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (10, 1, 0, 59, 55, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (11, 3, 2, 60, 52, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (12, 2, 0, 61, 56, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (13, 2, 1, 62, 51, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (14, 2, 1, 63, 54, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (15, 2, 1, 64, 57, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (16, 4, 3, 65, 50, 2018, 'Eighth-Final');
INSERT INTO public.games VALUES (17, 1, 0, 65, 66, 2014, 'Final');
INSERT INTO public.games VALUES (18, 3, 0, 56, 67, 2014, 'Third Place');
INSERT INTO public.games VALUES (19, 1, 0, 67, 65, 2014, 'Semi-Final');
INSERT INTO public.games VALUES (20, 7, 1, 56, 66, 2014, 'Semi-Final');
INSERT INTO public.games VALUES (21, 1, 0, 68, 67, 2014, 'Quarter-Final');
INSERT INTO public.games VALUES (22, 1, 0, 52, 65, 2014, 'Quarter-Final');
INSERT INTO public.games VALUES (23, 2, 1, 58, 56, 2014, 'Quarter-Final');
INSERT INTO public.games VALUES (24, 1, 0, 50, 66, 2014, 'Quarter-Final');
INSERT INTO public.games VALUES (25, 2, 1, 69, 56, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (26, 2, 0, 57, 58, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (27, 2, 0, 70, 50, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (28, 2, 1, 71, 66, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (29, 2, 1, 61, 67, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (30, 2, 1, 72, 68, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (31, 1, 0, 59, 65, 2014, 'Eighth-Final');
INSERT INTO public.games VALUES (32, 2, 1, 73, 52, 2014, 'Eighth-Final');


--
-- Data for Name: teams; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.teams VALUES (50, 'France');
INSERT INTO public.teams VALUES (51, 'Croatia');
INSERT INTO public.teams VALUES (52, 'Belgium');
INSERT INTO public.teams VALUES (53, 'England');
INSERT INTO public.teams VALUES (54, 'Russia');
INSERT INTO public.teams VALUES (55, 'Sweden');
INSERT INTO public.teams VALUES (56, 'Brazil');
INSERT INTO public.teams VALUES (57, 'Uruguay');
INSERT INTO public.teams VALUES (58, 'Colombia');
INSERT INTO public.teams VALUES (59, 'Switzerland');
INSERT INTO public.teams VALUES (60, 'Japan');
INSERT INTO public.teams VALUES (61, 'Mexico');
INSERT INTO public.teams VALUES (62, 'Denmark');
INSERT INTO public.teams VALUES (63, 'Spain');
INSERT INTO public.teams VALUES (64, 'Portugal');
INSERT INTO public.teams VALUES (65, 'Argentina');
INSERT INTO public.teams VALUES (66, 'Germany');
INSERT INTO public.teams VALUES (67, 'Netherlands');
INSERT INTO public.teams VALUES (68, 'Costa Rica');
INSERT INTO public.teams VALUES (69, 'Chile');
INSERT INTO public.teams VALUES (70, 'Nigeria');
INSERT INTO public.teams VALUES (71, 'Algeria');
INSERT INTO public.teams VALUES (72, 'Greece');
INSERT INTO public.teams VALUES (73, 'United States');


--
-- Name: games_game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.games_game_id_seq', 32, true);


--
-- Name: teams_team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: freecodecamp
--

SELECT pg_catalog.setval('public.teams_team_id_seq', 73, true);


--
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_pkey PRIMARY KEY (game_id);


--
-- Name: teams teams_name_key; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_name_key UNIQUE (name);


--
-- Name: teams teams_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (team_id);


--
-- Name: games fk_opponent_id; Type: FK CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT fk_opponent_id FOREIGN KEY (opponent_id) REFERENCES public.teams(team_id);


--
-- Name: games fk_winner_id; Type: FK CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT fk_winner_id FOREIGN KEY (winner_id) REFERENCES public.teams(team_id);


--
-- PostgreSQL database dump complete
--

